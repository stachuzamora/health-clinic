package edu.ib.health_clinic_reservation.config;

import edu.ib.health_clinic_reservation.security.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;
    private final PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    public WebSecurityConfig(DataSource dataSource, PasswordEncoderConfig passwordEncoderConfig) {
        this.dataSource = dataSource;
        this.passwordEncoderConfig = passwordEncoderConfig;

    }

    public WebSecurityConfig(boolean disableDefaults, DataSource dataSource, PasswordEncoderConfig passwordEncoderConfig) {
        super(disableDefaults);
        this.dataSource = dataSource;
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers( "/rezerwacja").hasAnyRole("ADMIN", "USER")
                .antMatchers("/moje_rezerwacje/**", "/moje_rezerwacje_dodaj/**", "/getAppointment").hasRole("USER")
                .antMatchers("/h2/**").permitAll()
                .antMatchers("/przychodnie").permitAll()
                .antMatchers("/").permitAll()
//                .antMatchers("/getAppointment").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/rezerwacja")
//                .failureForwardUrl("/login)
                .permitAll()
                .and()
                .logout()
//                .logoutUrl("/logout")
                .permitAll()
                .and()
                .csrf().disable();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("{noop}admin")
//                .roles("ADMIN");

        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select LOGIN, PASSWORD_HASHED, ENABLED from PATIENT where LOGIN = ?")
                .authoritiesByUsernameQuery("select LOGIN, ROLE from AUTHORITY where LOGIN = ?");
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/img/**", "/h2/**", "/css/**");
    }
}
