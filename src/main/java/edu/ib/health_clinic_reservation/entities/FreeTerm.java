package edu.ib.health_clinic_reservation.entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class FreeTerm {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;
    private Date date;
    private String time;
    private String specialty;
    @OneToOne
    private Doctor doctor;
    @OneToOne
    private Facility facility;


    public FreeTerm() {
    }

    public FreeTerm(Date date, String time, String specialty, Doctor doctor, Facility facility) {
        this.date = date;
        this.time = time;
        this.specialty = specialty;
        this.doctor = doctor;
        this.facility = facility;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    @Override
    public String toString() {
        return "FreeTerm{" +
                "id=" + id +
                ", date=" + date +
                ", time='" + time +
                ", specialty='" + specialty +
                ", doctor=" + doctor +
                ", facility=" + facility +
                '}';
    }
}
