package edu.ib.health_clinic_reservation.entities;


import javax.persistence.*;
import java.sql.Date;

@Entity
//@Table(name = "Appointment")
public class Appointment {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private Date date;
    private String appointmentTime;
    private String specialty;
    private Long doctorId;
    private String doctorName;
    private String shortDescription;
    private String facilityName;
    private String facilityCity;
    private boolean isFinished;
    @ManyToOne
    private PatientDTO patientDTO;

    public Appointment() {
    }

    public Appointment(Date date, String appointmentTime, String specialty, Long doctorId, String doctorName, String shortDescription, String facilityName, String facilityCity, boolean isFinished, PatientDTO patientDTO) {
        this.date = date;
        this.appointmentTime = appointmentTime;
        this.specialty = specialty;
        this.doctorId = doctorId;
        this.doctorName = doctorName;
        this.shortDescription = shortDescription;
        this.facilityName = facilityName;
        this.facilityCity = facilityCity;
        this.isFinished = isFinished;
        this.patientDTO = patientDTO;
    }

    public Appointment(Date date, String appointmentTime, String specialty, Long doctorId, String doctorName, String facilityName, String facilityCity, PatientDTO patientDTO) {
        this.date = date;
        this.appointmentTime = appointmentTime;
        this.specialty = specialty;
        this.doctorName = doctorName;
        this.doctorId = doctorId;
        this.facilityName = facilityName;
        this.facilityCity = facilityCity;
        this.patientDTO = patientDTO;
    }


    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void addNewAppointment(Date date, String appointmentTime, Doctor doctor, Facility facility) {
        this.date = date;
        this.appointmentTime = appointmentTime;
        this.doctorId = doctor.getId();
        this.facilityName = facility.getName();
    }

    public String getFacilityCity() {
        return facilityCity;
    }

    public void setFacilityCity(String facilityCity) {
        this.facilityCity = facilityCity;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", date=" + date +
                ", appointmentTime='" + appointmentTime + '\'' +
                ", specialty='" + specialty + '\'' +
                ", doctorId=" + doctorId +
                ", doctorName='" + doctorName + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", facilityName='" + facilityName + '\'' +
                ", facilityCity='" + facilityCity + '\'' +
                ", isFinished=" + isFinished +
                ", patientDTO=" + patientDTO +
                '}';
    }
}
