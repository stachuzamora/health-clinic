package edu.ib.health_clinic_reservation.entities;

import javax.persistence.*;

@Entity
@Table(name = "Authority")
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Long;

    private String login;
    private String role;

    public Authority() {
    }

    public Authority(String login, String role) {
        this.login = login;
        this.role = role;
    }

    public int getLong() {
        return Long;
    }

    public void setLong(int aLong) {
        Long = aLong;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
