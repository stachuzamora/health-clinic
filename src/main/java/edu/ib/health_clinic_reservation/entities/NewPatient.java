package edu.ib.health_clinic_reservation.entities;

import edu.ib.health_clinic_reservation.util.Role;

public class NewPatient {

    private String login;
    private String name;
    private String lastName;
    private String password;
    private String passwordConfirm;
    private final Role role = Role.ROLE_USER;

    public NewPatient() {
    }

    public NewPatient(String login, String name, String lastName, String password, String passwordConfirm) {
        this.login = login;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public boolean validatePasswords() {
        return password.equals(passwordConfirm);
    }

    @Override
    public String toString() {
        return "NewPatient{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirm='" + passwordConfirm + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
    
}
