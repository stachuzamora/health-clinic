package edu.ib.health_clinic_reservation.entities;

import edu.ib.health_clinic_reservation.util.Role;

public class Patient {

    private String login;
    private String name;
    private String lastName;
    private String password;
    private final Role role = Role.ROLE_USER;

    public Patient(String login, String name, String lastName, String password) {
        this.login = login;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
    }

    public Patient() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }
}
