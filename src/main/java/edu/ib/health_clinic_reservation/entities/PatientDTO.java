package edu.ib.health_clinic_reservation.entities;

import edu.ib.health_clinic_reservation.security.PasswordEncoderConfig;
import edu.ib.health_clinic_reservation.util.Role;
import org.hibernate.annotations.Cascade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Patient")
public class PatientDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String name;
    private String lastName;
    private String passwordHashed;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private Set<Appointment> appointmentSet;
    private final boolean enabled = true;

    public PatientDTO() {
    }

    public PatientDTO(Long id, String login, String passwordHashed, Role role) {
        this.id = id;
        this.login = login;
        this.passwordHashed = passwordHashed;
    }

    public void addAppointment(Appointment appointment) {
        appointmentSet.add(appointment);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHashed() {
        return passwordHashed;
    }

    public void setPasswordHashed(String passwordHashed) {
        this.passwordHashed = passwordHashed;
    }

    public Set<Appointment> getAppointmentSet() {
        return appointmentSet;
    }

    public void setAppointmentSet(Set<Appointment> appointmentSet) {
        this.appointmentSet = appointmentSet;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", enabled=" + enabled +
                '}';
    }

    @Service
    public static class PatientDTOBuilder {
        private String login;
        private String name;
        private String lastName;
        private String passHash;

        PasswordEncoderConfig passwordEncoderConfig;

        @Autowired
        public PatientDTOBuilder(PasswordEncoderConfig passwordEncoderConfig) {
            this.passwordEncoderConfig = passwordEncoderConfig;
        }

        public PatientDTOBuilder() {

        }

        public PatientDTO patientToPatientDTO(Patient patient) {
            this.login = patient.getLogin();
            this.name = patient.getName();
            this.lastName = patient.getLastName();
            this.passHash = new PasswordEncoderConfig().passwordEncoder().encode(patient.getPassword());
            return new PatientDTO(this);

        }

    }

    public PatientDTO(PatientDTOBuilder patientDTOBuilder) {
        this.login = patientDTOBuilder.login;
        this.name = patientDTOBuilder.name;
        this.lastName = patientDTOBuilder.lastName;
        this.passwordHashed = patientDTOBuilder.passHash;
    }

}
