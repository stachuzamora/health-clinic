package edu.ib.health_clinic_reservation.entities;

import edu.ib.health_clinic_reservation.repository.FreeTermRepo;
import edu.ib.health_clinic_reservation.service.FreeTermService;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Doctor {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String name;
    private String lastName;
    @OneToOne
    private Facility facility;
    private String specialty;


    public Doctor() {
    }

    public Doctor(String name, String lastName, Facility facility, String specialty) {
        this.name = name;
        this.lastName = lastName;
        this.facility = facility;
        this.specialty = specialty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", facility='" + facility + '\'' +
                ", specialty='" + specialty + '\'' +
                '}';
    }

}
