package edu.ib.health_clinic_reservation.controllers;

import edu.ib.health_clinic_reservation.entities.Authority;
import edu.ib.health_clinic_reservation.entities.Patient;
import edu.ib.health_clinic_reservation.entities.PatientDTO;
import edu.ib.health_clinic_reservation.security.PasswordEncoderConfig;
import edu.ib.health_clinic_reservation.service.AuthorityService;
import edu.ib.health_clinic_reservation.service.PatientService;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private PatientService patientService;
    private AuthorityService authorityService;
    private PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    public RegistrationController(PatientService patientService, AuthorityService authorityService, PasswordEncoderConfig passwordEncoderConfig) {
        this.patientService = patientService;
        this.authorityService = authorityService;
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    /**
     * Metoda zwracająca widok do rejestracji pacjetna, dodatkowo
     * w celu poprawnego przekazania danych do serwera dodawany
     * jest obiekt newPatient, który jest wykorzystywany przez metode
     * createAccount do stworzenia nowego konta
     * @param newPatient
     * @param model
     * @return
     */
    @GetMapping("/rejestracja")
    public String getRegistrationPage(Patient newPatient, Model model){
//        ModelAndView modelAndView = new ModelAndView();
////        modelAndView.addObject("newPatient",new Patient());

        model.addAttribute("newPatient", newPatient);
//        modelAndView.setViewName("registration_page");
        return "registration_page";
    }

    /**
     * Metoda obsługująca rejestrację pacjetna, sprawdzane są warunki
     * czy pola nie są puste oraz czy podana nazwa użytkownika istnieje.
     * W momencie, gdy wszystko przejdzie kontrolę, konto zostaje zapisane w bazie
     * danych z uprawnieniami ROLE_USER, a zwracana jest strona logowania
     * @param newPatient
     * @param bindingResult
     * @param model
     * @return
     */
    @PostMapping("/rejestracja")
    public String createAccount(@Valid @ModelAttribute("newPatient") Patient newPatient, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) return "registration_page";
        else {

            String username = "";
            String name = "";
            String lastName = "";
            String password = "";


            username = newPatient.getLogin();
            name = newPatient.getName();
            lastName = newPatient.getLastName();
            password = newPatient.getPassword();

            if(username.isEmpty() || name.isEmpty() || lastName.isEmpty() || password.isEmpty()) {
                model.addAttribute("emptyFields", true);
                return "registration_page";
            }

            if(!(patientService.checkIfPatientExists(username))) {
                PatientDTO patientDTO = new PatientDTO.PatientDTOBuilder().patientToPatientDTO(newPatient);
                Authority authority = new Authority(username, newPatient.getRole().toString());

                patientService.createPatient(patientDTO);
                authorityService.addPatientAuthority(authority);
                model.addAttribute("succReg", true);
                return "login_page";
            } else {
                model.addAttribute("userExists", true);
                return "registration_page";
            }
        }
    }
}
