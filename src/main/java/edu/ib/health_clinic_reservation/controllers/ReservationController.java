package edu.ib.health_clinic_reservation.controllers;


import edu.ib.health_clinic_reservation.entities.*;
import edu.ib.health_clinic_reservation.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ReservationController {

    private AppointmentService appointmentService;
    private PatientService patientService;
    private DoctorService doctorService;
    private FreeTermService freeTermService;
    private FacilityService facilityService;

    @Autowired
    public ReservationController(AppointmentService appointmentService, PatientService patientService,
                                 DoctorService doctorService, FreeTermService freeTermService, FacilityService facilityService) {
        this.appointmentService = appointmentService;
        this.patientService = patientService;
        this.doctorService = doctorService;
        this.freeTermService = freeTermService;
        this.facilityService = facilityService;
    }

    /**
     * Metoda do wyświetlania rezerwacji pacjenta, zwracająca listę
     * wszystkich rezerwacji.
     * @param model
     * @return
     */
    @GetMapping("/moje_rezerwacje")
    public String getPatientReservationsPage(Model model) {
        String patientLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(patientLogin);


        List<Appointment> appointmentList = new ArrayList<>(patientDTO.getAppointmentSet());
        if (!appointmentList.isEmpty()) {
            Appointment appointment = appointmentList.get(appointmentList.size() - 1);
            model.addAttribute("doctor", appointment.getDoctorName());
            model.addAttribute("date", appointment.getDate());
            model.addAttribute("time", appointment.getAppointmentTime());
        }

        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));

        model.addAttribute("patient", patientDTO);
        return "patient_reservation";

    }


    /**
     * Metoda obsługująca zapytanie o nadchodzące rezerwacje, sprawdzająca
     * w bazie danych wszystkie rezerwacje i zwracające listę tylko tych, które
     * nie zostały zakończone
     * @param model
     * @return
     */
    @GetMapping("/moje_rezerwacje_nadchodzace")
    public String showUpcoming(Model model) {

        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(user);

        List<Appointment> appointmentList = new ArrayList<>(patientDTO.getAppointmentSet());
        if (!appointmentList.isEmpty()) {
            Appointment appointment = appointmentList.get(appointmentList.size() - 1);
            model.addAttribute("doctor", appointment.getDoctorName());
            model.addAttribute("date", appointment.getDate());
            model.addAttribute("time", appointment.getAppointmentTime());
            List<Appointment> appointments = patientService.getUpcomingAppointments(user);
            patientDTO.getAppointmentSet().clear();
            patientDTO.getAppointmentSet().addAll(appointments);
            model.addAttribute("patient", patientDTO);
        }
        return "patient_reservation";
    }

    /**
     * Metoda obsługująca zapytanie o nadchodzące rezerwacje, sprawdzająca
     * w bazie danych wszystkie rezerwacje i zwracające listę tylko tych, które
     * zostały zakończone
     * @param model
     * @return
     */
    @GetMapping("/moje_rezerwacje_zakonczone")
    public String showFinished(Model model) {
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(user);


        List<Appointment> appointmentList = new ArrayList<>(patientDTO.getAppointmentSet());
        if (!appointmentList.isEmpty()) {
            Appointment appointment = appointmentList.get(appointmentList.size() - 1);
            model.addAttribute("doctor", appointment.getDoctorName());
            model.addAttribute("date", appointment.getDate());
            model.addAttribute("time", appointment.getAppointmentTime());
            List<Appointment> appointments = patientService.getFinishedAppointments(user);
            patientDTO.getAppointmentSet().clear();
            patientDTO.getAppointmentSet().addAll(appointments);
            model.addAttribute("patient", patientDTO);
        }
        return "patient_reservation";
    }

    /**
     * Metoda obsługująca anulowanie rezerwacji, podana rezerwacja jest wyciągan z
     * bazy danych, usuwana a następnie na jej podobieństwo tworzony jest wolny termin
     * i zapisywany w bazie danych
     * @param appointmentId
     * @param doctorId
     * @param model
     * @return
     */
    @GetMapping("/anuluj_rezerwacje")
    public String cancelReservation(@RequestParam Long appointmentId, @RequestParam(name = "doctorId") Long doctorId, Model model){

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(login);


        List<Appointment> appointments = new ArrayList<>(patientDTO.getAppointmentSet());
        Appointment appointment = appointmentService.findById(appointmentId).get();
        appointments.remove(appointment);
        patientDTO.getAppointmentSet().clear();
        patientDTO.getAppointmentSet().addAll(appointments);
        appointmentService.deleteAppointment(appointmentId);
        Doctor doctor = doctorService.getDoctorById(doctorId);

        System.out.println(doctor.toString());

        freeTermService.addNewTerm(appointment.getDate(), appointment.getAppointmentTime()
            , doctor.getSpecialty(), doctor, doctor.getFacility());
//        Facility facility = facilityService.findByName(appointment.getFacilityName());
//
//        freeTermService.addNewTerm(appointment.getDate(), appointment.getAppointmentTime(), appointment.getSpecialty(),
//            doctor, facility);

        return getPatientReservationsPage(model);
    }
}
