package edu.ib.health_clinic_reservation.controllers;


import edu.ib.health_clinic_reservation.entities.Appointment;
import edu.ib.health_clinic_reservation.entities.FreeTerm;
import edu.ib.health_clinic_reservation.entities.PatientDTO;
import edu.ib.health_clinic_reservation.service.AppointmentService;
import edu.ib.health_clinic_reservation.service.AuthorityService;
import edu.ib.health_clinic_reservation.service.FreeTermService;
import edu.ib.health_clinic_reservation.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;
import java.sql.Date;
import java.time.LocalDate;

@Controller
public class LoginController {

    AuthorityService authorityService;
    FreeTermService freeTermService;
    AppointmentService appointmentService;
    PatientService patientService;

    @Autowired
    public LoginController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }


    //    @RequestMapping(path = "/login", method = RequestMethod.POST)
//    public String loginPage() {
//        return "login";
//    }

//    @GetMapping("/admin_login")
//    public String login()


    /**
     * Metoda zwracająca customową strone logowania
     * @return
     */
    @GetMapping("/login")
    public String getLoginPage() {
        return "login_page";
    }
//

    /**
     * Metoda zwracająca strone panelu klienta lub przychodni,
     * przekazująca do modelu nazwę aktualnie zalogowanego użytkownika
     * wysyłając również listę jego rezerwacji wizyt
     * @param model
     * @return
     */
    @GetMapping("/rezerwacja")
    public String clinicAdminPage(Model model) {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        System.out.println(username);
        String role = authorityService.getUserRole(username);

        if (role.equals("ROLE_USER")) {
            model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
            model.addAttribute("specialty", "");
//        List<Appointment> appointments = new ArrayList<>();
//        appointments.add(new Appointment());
            model.addAttribute("freeTerms",new ArrayList<>());
            return "clinic_operations";
        } else if(role.equals("ROLE_ADMIN")) {

//            System.out.println(username);
//            PatientDTO patientDTO = patientService.getPatientByLogin(username);
//            System.out.println(patientDTO);
//            String facility = patientService.getPatientByLogin(username).getName();
//            String facilityId = patientService.getPatientByLogin(username).getLastName();

//            List<FreeTerm> freeTerms = freeTermService.getAllFreeTerms(Date.valueOf(LocalDate.now()), facility);

            model.addAttribute("freeTerms", new ArrayList<>());

//            List<Appointment> appointments = appointmentService.getAllAppointments(Date.valueOf(LocalDate.now()), facility);

            model.addAttribute("appt", new ArrayList<>());

            model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));

            return "clinic_operations";
        }


//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute("appt", new ArrayList<>());
        return "clinic_operations";
    }



//    @GetMapping("/logout")
//    public String logOut(Model model) {
//        model.addAttribute("loggedout", true);
//        return "index";
//    }

}
