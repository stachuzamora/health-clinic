package edu.ib.health_clinic_reservation.controllers;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    /**
     * Metoda zwracająca główną stronę
     * @return
     */
    @GetMapping("/")
    public String mainPage() {
        return "index";
    }

    /**
     * Metoda zwracająca nazwe widoku do wyświetlenia w momencie,
     * gdy użytkownik chce zobaczyć dostępne przychodnie
     * @param model
     * @return
     */
    @GetMapping("/przychodnie")
    public String clinicsPage(Model model) {
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean isLoggedIn = !user.equals("anonymousUser");
        model.addAttribute("isLoggedIn", isLoggedIn);
        return "clinics";
    }

}
