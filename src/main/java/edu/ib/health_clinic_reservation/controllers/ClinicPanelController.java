package edu.ib.health_clinic_reservation.controllers;

import edu.ib.health_clinic_reservation.entities.*;
import edu.ib.health_clinic_reservation.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class ClinicPanelController {

    private PatientService patientService;
    private AppointmentService appointmentService;
    private FreeTermService freeTermService;
    private DoctorService doctorService;
    private FacilityService facilityService;

    @Autowired
    public ClinicPanelController(PatientService patientService, AppointmentService appointmentService,
                                 FreeTermService freeTermService, DoctorService doctorService,
                                 FacilityService facilityService) {
        this.patientService = patientService;
        this.appointmentService = appointmentService;
        this.freeTermService = freeTermService;
        this.doctorService = doctorService;
        this.facilityService = facilityService;
    }



    @GetMapping("/clinics")
    public String clinics() {
        return "clinics";
    }


    /**
     * Metoda zwracajaca liste nadchodzacych terminów na dany dzień dla przychodni,
     * która jest aktualnie załadowana
     * @param model
     * @return
     */
    @GetMapping("/pokaz_nadchodzace")
    public String upcoming(Model model){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        PatientDTO patientDTO = patientService.getPatientByLogin(username);


        List<Appointment> appointmentList = appointmentService.getAllAppointments(Date.valueOf(LocalDate.now()), patientDTO.getName());
        appointmentList = appointmentList.stream().sorted(Comparator.comparing(appointment -> Integer.parseInt(appointment.getAppointmentTime().split(":")[0]))).collect(Collectors.toList());
        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        model.addAttribute("appt", appointmentList);

        return model.getAttribute("freeTerm") != null ? "clinic_operations" : showFreeTerms(model);
    }


    /**
     * Metoda obsługująca anulowanie wizyty, rekord o podanych parametrach jest znajdowany
     * w tabeli z wizytami, usuwany a następnie nowy obiekt klasy FreeTerm jest tworzony
     * i zapisywany w dniu i o godzinie odpowiadającej usuniętej wizycie
     * @param id
     * @param date
     * @param time
     * @param doctorId
     * @param model
     * @return
     */
    @GetMapping("/anuluj_wizyte")
    public String cancellAppointment(@RequestParam(name = "apptId") Long id, @RequestParam(name = "date") Date date,
                                     @RequestParam(name = "time") String time, @RequestParam(name = "doctorId") Long doctorId, Model model) {

        Optional<Appointment> appointment = appointmentService.findById(id);

        PatientDTO patientDTO;

        if(appointment.isPresent()) {
            patientDTO = appointment.get().getPatientDTO();

            List<Appointment> appointments = new ArrayList<>(patientDTO.getAppointmentSet());
            appointments.remove(appointment.get());
            patientDTO.getAppointmentSet().clear();
            patientDTO.getAppointmentSet().addAll(appointments);
            appointmentService.deleteAppointment(id);
            Doctor doctor = doctorService.getDoctorById(appointment.get().getDoctorId());
            Facility facility = facilityService.findByName(appointment.get().getFacilityName());

            freeTermService.addNewTerm(appointment.get().getDate(), appointment.get().getAppointmentTime(), appointment.get().getSpecialty(),
                    doctor, facility);
        }


        return upcoming(model);
    }

    /**
     * Metoda ustawiająca status wizyty na zakończony. Wyciagany obiekt o podanych
     * parametrach zostaje nadpisany i uaktaulniony w bazie danych
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/zakoncz_wizyte")
    public String markAsEnded(@RequestParam (name = "apptId") Long id, Model model) {

        Optional<Appointment> appointment = appointmentService.findById(id);

        appointment.ifPresent(appointment1 -> appointment.get().setFinished(true));

        appointmentService.save(appointment.get());



//        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        return upcoming(model);

    }


    /**
     * Metoda wyszukująca wolne terminy w aktualnym dniu dla zalgowanego
     * administratora przychodni
     * @param model
     * @return
     */
    @GetMapping("/pokaz_wolne_terminy")
    public String showFreeTerms(Model model) {


        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(username);

        List<FreeTerm> freeTerms = freeTermService.getAllFreeTermsFacility(Date.valueOf(LocalDate.now()), patientDTO.getName());

        freeTerms = freeTerms.stream().sorted(Comparator.comparing(freeTerm -> Integer.parseInt(freeTerm.getTime().split(":")[0]))).collect(Collectors.toList());

        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        model.addAttribute("freeTerm", freeTerms);


        return model.getAttribute("appt") != null ? "clinic_operations" : upcoming(model);
    }


    /**
     * Metoda zwracająca listę wszystkich lekarzy, jacy pracują
     * w przychodni, której administrator jest zalogowany
     * @param model
     * @return
     */
    @GetMapping("/lekarze")
    public String getDoctorsPage(Model model) {

        String user = SecurityContextHolder.getContext().getAuthentication().getName();

        PatientDTO patientDTO = patientService.getPatientByLogin(user);

        Facility facility = facilityService.findByName(patientDTO.getName());

        List<Doctor> doctors = doctorService.getAllDoctors(facility);
        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        model.addAttribute("doctors", doctors);
        return "doctors_page";
    }

    /**
     * Metoda obsługująca zapytanie POST, administrator dodaje wolne
     * terminy na określoną ilość dni dla wybranego przez siebie lekarza.
     * @param doctorId
     * @param days
     * @param date
     * @param model
     * @return
     */
    @PostMapping("/dodaj_termin")
    public String addNewTermin(@RequestParam(name = "select") Long doctorId, @RequestParam (name = "days") int days, @RequestParam(name = "date") Date date, Model model){
        System.out.println(doctorId + " " + days);

        Doctor doctor = doctorService.getDoctorById(doctorId);

        doctorService.fillInDoctorTerms(doctor, date, days);

        return getDoctorsPage(model);

    }
}
