package edu.ib.health_clinic_reservation.controllers;

import edu.ib.health_clinic_reservation.entities.PatientDTO;
import edu.ib.health_clinic_reservation.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PatientProfileController {

    private PatientService patientService;

    @Autowired
    public PatientProfileController(PatientService patientService) {
        this.patientService = patientService;
    }


    /**
     * Metoda zwracająca strone z profilem pacjenta, dodatkowo
     * umieszczana w modelu jest nazwa użytkownika
     * @param model
     * @return
     */
    @GetMapping("/profil_pacjenta")
    public String getProfilePage(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(username);
        model.addAttribute("patient", patientDTO);
        return "patient_profile";
    }
}
