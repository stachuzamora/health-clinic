package edu.ib.health_clinic_reservation.controllers;


import edu.ib.health_clinic_reservation.entities.Appointment;
import edu.ib.health_clinic_reservation.entities.Doctor;
import edu.ib.health_clinic_reservation.entities.FreeTerm;
import edu.ib.health_clinic_reservation.entities.PatientDTO;
import edu.ib.health_clinic_reservation.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AppointmentsController {

    private final AppointmentService appointmentService;
    private final FacilityService facilityService;
    private final FreeTermService freeTermService;
    private final DoctorService doctorService;
    private final PatientService patientService;

    @Autowired
    public AppointmentsController(AppointmentService appointmentService, FacilityService facilityService,
                                  FreeTermService freeTermService, DoctorService doctorService,
                                  PatientService patientService) {
        this.appointmentService = appointmentService;
        this.facilityService = facilityService;
        this.freeTermService = freeTermService;
        this.doctorService = doctorService;
        this.patientService = patientService;
    }

    /**
     * Metoda ta służy do wyciągania z bazy danych listy wolnych terminów zgodnie
     * z przekazanymi przez pacjenta parametrami, przekazując je do modelu. Zwraca
     * string'a z nazwą widoku do wyświetlenia
     * @param date
     * @param specialty
     * @param facility
     * @param model
     * @return
     */

    @GetMapping("/showAppointments")
    public String getFreeAppointments(@RequestParam Date date, @RequestParam String specialty, @RequestParam Long facility, Model model) {

//        String specialty = MedicalSpecialty.KARDIOLOG.toString();
//

//        Iterator<Appointment>freeAppointments = appointmentService.getFreeAppointments(date, specialty);
        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        model.addAttribute("specialty", specialty);
        model.addAttribute("date", date);

//        Appointment appointment = new Appointment(date, "15.30", specialty, 1L, "Natalia Klepacz","", 1L, "Wrocław", false);
//        Appointment appointment2= new Appointment(date, "16.00", specialty, 1L, "Natalia Klepacz","", 1L, "Wrocław", false);
//        Appointment appointment3 = new Appointment(date, "16.30", specialty, 1L, "Natalia Klepacz","", 1L, "Wrocław", false);


        List<FreeTerm> freeTermList = new ArrayList<>();
        if(facility == 5) {
            freeTermList = freeTermService.getAllFreeTerms(date, specialty);
        } else {
            try {
                freeTermList = freeTermService.getAllFreeTermsWithFacility(date, specialty, facility);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        freeTermList = freeTermList.stream().sorted(Comparator.comparing(freeTerm -> Integer.parseInt(freeTerm.getTime().split(":")[0]))).collect(Collectors.toList());

//        List<Doctor> doctors = freeTermList.stream().map(FreeTerm::getDoctor).collect(Collectors.toList());
//        List<Facility> facilities = freeTermList.stream().map(FreeTerm::getFacility).collect(Collectors.toList());

        model.addAttribute("freeTerms", freeTermList);
//        model.addAttribute("doctors", doctors);
//        model.addAttribute("facilities", facilities);
//        model.addAttribute("tableSort", "visible");
        return "clinic_operations";
    }

    /**
     * Metoda obsługująca proces rezerwacji, podane parametry są przekazywane do metody,
     * w której tworzony jest nowy obiekt klasy Appointment (wizyta), dodawany
     * jest do bazy danych do tabeli z wizytami, a wolny termin jest usuwany z odpowiadającej
     * mu tabeli. Metoda zwraca string'a z nazwą widoku rezerwacji pacjenta
     * @param freeTermId
     * @param date
     * @param time
     * @param doctorId
     * @param model
     * @return
     */

    @GetMapping("/moje_rezerwacje_dodaj")
    public String makeAppointment(@RequestParam Long freeTermId, @RequestParam Date date, @RequestParam String time, @RequestParam Long doctorId, Model model) {

        // getting currently logged in user
        String patientLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        PatientDTO patientDTO = patientService.getPatientByLogin(patientLogin);

        // getting doctor from database
        Doctor doctor = doctorService.getDoctorById(doctorId);




            if(patientService.checkForTimeDateAppointment(patientLogin, date, time)){
                model.addAttribute("appointmentAlert", true);
                model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
                model.addAttribute("specialty", doctor.getSpecialty());
                model.addAttribute("date", date);
                model.addAttribute("freeTerms", new ArrayList<>());
                return "clinic_operations";
            }

            if(patientService.checkNumberOfAppointments(patientLogin, date)) {
                model.addAttribute("dayLimit", true);
                model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
                model.addAttribute("specialty", doctor.getSpecialty());
                model.addAttribute("date", date);
                model.addAttribute("freeTerms", new ArrayList<>());
                return "clinic_operations";
            }

        // deleting free term from database
        freeTermService.deleteFreeTermById(freeTermId);





        // creating new appointment based on free term and saving it in database
        Appointment appointment = new Appointment(date, time, doctor.getSpecialty(), doctor.getId(), doctorService.getDoctorName(doctorId),
                doctor.getFacility().getName(), doctor.getFacility().getCity(), patientDTO);
        appointmentService.addNewAppointment(appointment);
        System.out.println(appointment.toString());
        patientDTO.getAppointmentSet().add(appointment);
        patientService.save(patientDTO);

        model.addAttribute("date", date);
        model.addAttribute("time", time);
        model.addAttribute("todayDate", Date.valueOf(LocalDate.now()));
        model.addAttribute("doctor", doctorService.getDoctorName(doctorId));
        model.addAttribute("patient", patientDTO);
        model.addAttribute("freeTerms", new ArrayList<>());

        return "patient_reservation";
    }
}
