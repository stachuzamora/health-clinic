package edu.ib.health_clinic_reservation.util;

public enum Role {

    ROLE_USER, ROLE_ADMIN;

}
