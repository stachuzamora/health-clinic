package edu.ib.health_clinic_reservation.util;

public enum MedicalSpecialty {

    INTERNISTA, GINEKOLOG, WIRUSOLOG, OKULISTA, NEFROLOG, KARDIOLOG, PEDIATRA, ORTOPEDA, DIABETOLOG
}
