package edu.ib.health_clinic_reservation;

import edu.ib.health_clinic_reservation.config.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthClinicReservationApplication {

    public static void main(String[] args) {
        SpringApplication.run(HealthClinicReservationApplication.class, args);

    }

//    protected Class<?>[] getRootConfigClasses(){
//        return new Class[] {WebSecurityConfig.class};
//    }
}
