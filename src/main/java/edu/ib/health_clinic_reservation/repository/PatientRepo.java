package edu.ib.health_clinic_reservation.repository;

import edu.ib.health_clinic_reservation.entities.PatientDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepo extends JpaRepository<PatientDTO, Long> {

    /**
     * Metoda zwracaja obiekt pacjenta wg nazwy uzytkownika
     * @param login
     * @return
     */
    PatientDTO getPatientDTOByLogin(String login);

}
