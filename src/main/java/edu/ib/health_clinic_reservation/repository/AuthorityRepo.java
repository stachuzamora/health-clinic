package edu.ib.health_clinic_reservation.repository;


import edu.ib.health_clinic_reservation.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepo extends JpaRepository <Authority, Long> {

    /**
     * Metoda zwracająca obiekt klasy Authority wg loginu użytkwonika
     * @param login
     * @return
     */
    public Authority findAuthorityByLogin(String login);
}
