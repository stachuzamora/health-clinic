package edu.ib.health_clinic_reservation.repository;

import edu.ib.health_clinic_reservation.entities.Facility;
import edu.ib.health_clinic_reservation.entities.FreeTerm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface FreeTermRepo extends JpaRepository<FreeTerm, Long> {

    /**
     * Metoda zwracająca liste wolnych terminów wg daty i specjalnosci
     * @param date
     * @param specialty
     * @return
     */
    List<FreeTerm> findAllByDateAndSpecialty(Date date, String specialty);
    /**
     * Metoda zwracająca liste wolnych terminów wg daty, specjalnosci i placowki
     * @param date
     * @param specialty
     * @param facility
     * @return
     */
    List<FreeTerm> findAllByDateAndSpecialtyAndFacility(Date date, String specialty, Facility facility);
    /**
     * Metoda zwracająca liste wolnych terminów wg daty i placowki
     * @param date
     * @param facility
     * @return
     */
    List<FreeTerm> findAllByDateAndFacility(Date date, Facility facility);
}
