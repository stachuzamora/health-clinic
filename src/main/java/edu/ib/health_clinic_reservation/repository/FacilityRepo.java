package edu.ib.health_clinic_reservation.repository;

import edu.ib.health_clinic_reservation.entities.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface FacilityRepo extends JpaRepository<Facility, Long> {

    /**
     * Metoda zwracajaca obiekt przychodni wg jej nazwy
     * @param name
     * @return
     */
    Facility getFacilityByName(String name);
}
