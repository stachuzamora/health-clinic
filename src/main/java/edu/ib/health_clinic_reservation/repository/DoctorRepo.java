package edu.ib.health_clinic_reservation.repository;

import edu.ib.health_clinic_reservation.entities.Doctor;
import edu.ib.health_clinic_reservation.entities.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Stream;

@Repository
public interface DoctorRepo extends JpaRepository<Doctor, Long> {

    /**
     * Metoda zwracająca listę lekarzy wg specjalności oraz placówki
     * @param specialty
     * @param facility
     * @return
     */
    List<Doctor> findAllBySpecialtyAndFacility(String specialty, String facility);

    /**
     * Metoda zwracająca listę lekarzy wg specjalności
     * @param specialty
     * @return
     */
    List<Doctor> findAllBySpecialty(String specialty);

    /**
     * Metoda zwracająca obiekt klasy Doctor wg podanego unikatowego ID
     * @param id
     * @return
     */
    Doctor findDoctorById(Long id);

    /**
     * Metoda zwracajaca liste lekarzy dla danej placowki
     * @param facility
     * @return
     */
    List<Doctor> findAllByFacility(Facility facility);
}
