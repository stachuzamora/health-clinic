package edu.ib.health_clinic_reservation.repository;

import edu.ib.health_clinic_reservation.entities.Appointment;
import edu.ib.health_clinic_reservation.entities.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.sql.Date;

@Repository
public interface AppointmentRepo extends JpaRepository<Appointment, Long> {

    /**
     * Metoda zwracająca listę wizyt wg daty oraz nazwy placówki
     * @param date
     * @param facility
     * @return
     */
    List<Appointment> findAllByDateAndFacilityName(Date date, String facility);
}
