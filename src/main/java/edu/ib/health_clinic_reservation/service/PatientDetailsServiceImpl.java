//package edu.ib.health_clinic_reservation.service;
//
//import edu.ib.health_clinic_reservation.entities.PatientDTO;
//import edu.ib.health_clinic_reservation.repository.PatientRepo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Collections;
//import java.util.Set;
//
//@Service
//public class PatientDetailsServiceImpl implements UserDetailsService {
//
//    private final PatientRepo patientRepo;
//
//    @Autowired
//    public PatientDetailsServiceImpl(PatientRepo patientRepo) {
//        this.patientRepo = patientRepo;
//    }
//
//    @Override
//    @Transactional(readOnly = true)
//    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
//
//        PatientDTO patientDTO = patientRepo.getPatientDTOByLogin(login);
//
//        if(patientDTO == null) throw new UsernameNotFoundException(login);
//
////        Set<SimpleGrantedAuthority> role = Collections.singleton(new SimpleGrantedAuthority(patientDTO.getRole()));
//
//        return new User(patientDTO.getLogin(), patientDTO.getPasswordHashed(), null);
//    }
//}
