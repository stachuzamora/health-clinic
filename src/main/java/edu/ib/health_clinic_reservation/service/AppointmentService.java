package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Appointment;
import edu.ib.health_clinic_reservation.repository.AppointmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AppointmentService {

    private AppointmentRepo appointmentRepo;
    private FacilityService facilityService;
    private DoctorService doctorService;

    @Autowired
    public AppointmentService(AppointmentRepo appointmentRepo, FacilityService facilityService, DoctorService doctorService) {
        this.appointmentRepo = appointmentRepo;
        this.facilityService = facilityService;
        this.doctorService = doctorService;
    }

    public List<Appointment> getAllAppointments(Date date, String facilityName) {

        return appointmentRepo.findAllByDateAndFacilityName(date, facilityName);
    }

    public Appointment addNewAppointment(Appointment appointment) {
        return appointmentRepo.save(appointment);
    }

    public void deleteAppointment(Long id) {
        Optional<Appointment> appointment = appointmentRepo.findById(id);
        appointment.ifPresent(value -> appointmentRepo.delete(value));
    }

    public Optional<Appointment> findById(Long id) {
        return appointmentRepo.findById(id);
    }


//    public Iterator<Appointment> getFreeAppointments(Date date, String specialty) {
//        Iterable<Appointment> allAppointments = appointmentRepo.findAllByDateAndSpecialty(date, specialty);
//        List<Appointment> appointments = StreamSupport.stream(allAppointments.spliterator(), false).collect(Collectors.toList());
//        if (appointments.size() == 0) {
//            return getAllDayAvailableAppointments(date, specialty);
//        }
//        return null;
//    }
//
//    private List<Appointment> getAllDayAvailableAppointments(Date date, String specialty) {
//

    public void save(Appointment appointment) {
        appointmentRepo.save(appointment);
    }
//
//
//    }

}
