package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Appointment;
import edu.ib.health_clinic_reservation.entities.Patient;
import edu.ib.health_clinic_reservation.entities.PatientDTO;
import edu.ib.health_clinic_reservation.repository.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.sql.Date;
import java.util.stream.Collectors;

@Service
public class PatientService {


    private PatientRepo patientRepo;

    @Autowired
    public PatientService(PatientRepo patientRepo) {
        this.patientRepo = patientRepo;
    }

    public PatientDTO getPatientByLogin(String login) {
        PatientDTO patientDTO = patientRepo.getPatientDTOByLogin(login);
        return patientDTO;
    }

    public boolean checkNumberOfAppointments(String login, Date date) {
        PatientDTO patient = patientRepo.getPatientDTOByLogin(login);
        int counter = (int) patient.getAppointmentSet().stream().filter(appointment -> appointment.getDate().equals(date)).count();

        return counter > 2;
    }

    public boolean checkForTimeDateAppointment(String login, Date date, String time) {
        PatientDTO patient = patientRepo.getPatientDTOByLogin(login);

        for (Appointment appointment : patient.getAppointmentSet()) {
            if(appointment.getDate().equals(date) && appointment.getAppointmentTime().equals(time)){
                return true;
            }
        }
        return false;
    }

    public void save(PatientDTO patientDTO) {
        patientRepo.save(patientDTO);
    }

    public List<Appointment> getUpcomingAppointments(String login) {
        PatientDTO patient = patientRepo.getPatientDTOByLogin(login);

        Set<Appointment> appointmentSet = patient.getAppointmentSet();

        return appointmentSet.stream().filter(appointment -> !appointment.isFinished()).collect(Collectors.toList());
    }

    public List<Appointment> getFinishedAppointments(String login) {
        PatientDTO patient = patientRepo.getPatientDTOByLogin(login);

        Set<Appointment> appointments = patient.getAppointmentSet();

        return appointments.stream().filter(Appointment::isFinished).collect(Collectors.toList());
    }

    public PatientDTO createPatient(PatientDTO patientDTO) {
        return patientRepo.save(patientDTO);
    }

    public boolean checkIfPatientExists(String login) {
        return patientRepo.getPatientDTOByLogin(login) != null;
    }
}
