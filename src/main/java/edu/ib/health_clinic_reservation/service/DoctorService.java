package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Doctor;
import edu.ib.health_clinic_reservation.entities.Facility;
import edu.ib.health_clinic_reservation.repository.DoctorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private DoctorRepo doctorRepo;
    private FreeTermService freeTermService;

    @Autowired
    public DoctorService(DoctorRepo doctorRepo, FreeTermService freeTermService) {
        this.doctorRepo = doctorRepo;
        this.freeTermService = freeTermService;
    }


    public List<Doctor> getDoctorsId(String specialty) {
        return doctorRepo.findAllBySpecialty(specialty);

    }

    public String getDoctorName(Long id) {
        Doctor doctor = doctorRepo.findDoctorById(id);
        String name = doctorRepo.findDoctorById(id).getName();
        String lastName = doctorRepo.findDoctorById(id).getLastName();
        return name + " " + lastName;
    }

    public void fillInDoctorTerms(Doctor doctor, Date date, int noOfDays) {
        freeTermService.fillInNewTerms(date, noOfDays, doctor);
    }

    public Doctor getDoctorById(Long id) {
        return doctorRepo.findDoctorById(id);
    }

    public List<Doctor> getAllDoctors(Facility facility) {
        return doctorRepo.findAllByFacility(facility);
    }


}
