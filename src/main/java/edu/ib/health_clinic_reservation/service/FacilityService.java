package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Facility;
import edu.ib.health_clinic_reservation.repository.FacilityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class FacilityService {

    private FacilityRepo facilityRepo;


    @Autowired
    public FacilityService(FacilityRepo facilityRepo) {
        this.facilityRepo = facilityRepo;
    }


    public int getBeginningHour(String name) {
        return facilityRepo.getFacilityByName(name).getBeginHour();
    }

    public int getEndingHour(String name) {
        return facilityRepo.getFacilityByName(name).getEndHour();
    }

    public long getFacilityId(String name) {
        return facilityRepo.getFacilityByName(name).getId();
    }

    public List<Facility> findAll() {
        return facilityRepo.findAll();
    }

    public Optional<Facility> findById(Long id){
        return facilityRepo.findById(id);
    }

    public Facility findByName(String name) {
//        System.out.println(facilityRepo.getFacilityByName(name));
        return facilityRepo.getFacilityByName(name);
    }

    public void save(){};
}
