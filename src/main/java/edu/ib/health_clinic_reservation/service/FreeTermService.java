package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Doctor;
import edu.ib.health_clinic_reservation.entities.Facility;
import edu.ib.health_clinic_reservation.entities.FreeTerm;
import edu.ib.health_clinic_reservation.repository.FreeTermRepo;
import edu.ib.health_clinic_reservation.service.FacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.sql.Date;
import java.util.Optional;

@Service
public class FreeTermService {

    private FreeTermRepo freeTermRepo;
    private FacilityService facilityService;

    @Autowired
    public FreeTermService(FreeTermRepo freeTermRepo, FacilityService facilityService) {
        this.freeTermRepo = freeTermRepo;
        this.facilityService = facilityService;
    }

    public void fillInNewTerms(Date from, int days, Doctor doctor) {


        Facility facility = doctor.getFacility();
//        System.out.println(facility);

        java.util.Date saveDate = from;
//        System.out.println(saveDate);

        for (int i = 0; i < days; i++) {

            List<FreeTerm> termsToSave = generateFreeTerms(from, doctor);

            termsToSave.forEach(freeTerm -> freeTermRepo.save(freeTerm));
            Calendar c = Calendar.getInstance();
            c.setTime(saveDate);
            c.add(Calendar.DATE, 1);
            saveDate = c.getTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            from = Date.valueOf(simpleDateFormat.format(saveDate));

        }

    }


    private List<FreeTerm> generateFreeTerms(Date date, Doctor doctor) {

        Facility docFacilty = doctor.getFacility();
        int beginHour = docFacilty.getBeginHour();
        int endHour = docFacilty.getEndHour();

//        System.out.println(beginHour);
//        System.out.println(endHour);
//        System.out.println(date);
//        System.out.println(doctor);
//        System.out.println(docFacilty);

        List<FreeTerm> freeTerms = new ArrayList<>();
        int hourCounter = -1;
        for (int i = 0; i < (endHour - beginHour) * 2; i++) {
            String halfAnHour = i%2 == 0 ? "00" : "30";
            hourCounter = i%2 == 0 ? hourCounter +1 : hourCounter;
            freeTerms.add(new FreeTerm(date, ((beginHour + hourCounter) + ":" + halfAnHour), doctor.getSpecialty(), doctor, doctor.getFacility()));
        }

        return freeTerms;
    }


    public List<FreeTerm> getAllFreeTerms(Date date, String specialty) {
        return freeTermRepo.findAllByDateAndSpecialty(date, specialty);
    }

    public List<FreeTerm> getAllFreeTermsFacility(Date date, String facilityName) {
        Facility facility = facilityService.findByName(facilityName);
        return freeTermRepo.findAllByDateAndFacility(date, facility);
    }

    public List<FreeTerm> getAllFreeTermsWithFacility(Date date, String specialty, Long facilityId) throws Exception {
        Optional<Facility> facility = facilityService.findById(facilityId);
        if(facility.isEmpty()) throw new Exception("Facility not found");
        return freeTermRepo.findAllByDateAndSpecialtyAndFacility(date, specialty, facility.get());
    }

    public void addNewTerm(Date date, String time, String specialty, Doctor doctor, Facility facility) {
        freeTermRepo.save(new FreeTerm(date, time, specialty, doctor, facility));
    }


    public void deleteFreeTermById(Long id) {
        freeTermRepo.deleteById(id);
    }

}
