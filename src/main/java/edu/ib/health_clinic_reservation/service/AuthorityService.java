package edu.ib.health_clinic_reservation.service;

import edu.ib.health_clinic_reservation.entities.Authority;
import edu.ib.health_clinic_reservation.repository.AuthorityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {

    private AuthorityRepo authorityRepo;

    @Autowired
    public AuthorityService(AuthorityRepo authorityRepo) {
        this.authorityRepo = authorityRepo;
    }

    public Authority addPatientAuthority(Authority authority){
        return authorityRepo.save(authority);
    }

    public String getUserRole(String username) {
        return authorityRepo.findAuthorityByLogin(username).getRole();
    }
}
