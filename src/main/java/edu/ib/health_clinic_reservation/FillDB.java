package edu.ib.health_clinic_reservation;

import edu.ib.health_clinic_reservation.entities.*;
import edu.ib.health_clinic_reservation.repository.*;
import edu.ib.health_clinic_reservation.service.FreeTermService;
import edu.ib.health_clinic_reservation.util.MedicalSpecialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.*;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashSet;

@Component
public class FillDB {

    private final PatientRepo patientRepo;
    private final AuthorityRepo authorityRepo;
    private final DoctorRepo doctorRepo;
    private final AppointmentRepo appointmentRepo;
    private final FacilityRepo facilityRepo;
    private final FreeTermService freeTermService;
    private final FreeTermRepo freeTermRepo;


    @Autowired
    public FillDB(PatientRepo patientRepo, AuthorityRepo authorityRepo, DoctorRepo doctorRepo,
                  AppointmentRepo appointmentRepo, FacilityRepo facilityRepo, FreeTermService freeTermService,
                  FreeTermRepo freeTermRepo) {
        this.patientRepo = patientRepo;
        this.authorityRepo = authorityRepo;
        this.doctorRepo = doctorRepo;
        this.appointmentRepo = appointmentRepo;
        this.facilityRepo = facilityRepo;
        this.freeTermService = freeTermService;
        this.freeTermRepo = freeTermRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDatabaseWithUsers() {


        // PATIENT SECTION
        // *************************************************************************************************************

        Patient patient = new Patient("pacjent", "Jan", "Kowalski", "hassword");
        PatientDTO patientDTO = new PatientDTO.PatientDTOBuilder().patientToPatientDTO(patient);

        Patient admin = new Patient("admin", "Przy Jagiellońskiej", "1", "admin");
        PatientDTO adminDto = new PatientDTO.PatientDTOBuilder().patientToPatientDTO(admin);

        Authority patientAuthority = new Authority(patient.getLogin(), patient.getRole().toString());
        Authority adminAuthority = new Authority(admin.getLogin(),"ROLE_ADMIN");

        Patient patient1 = new Patient("pacjentt", "Majki", "Majk", "haslo");
        Authority patient1Auth = new Authority(patient1.getLogin(), patient1.getRole().toString());

        patientRepo.save(patientDTO);
        patientRepo.save(adminDto);
        patientRepo.save(new PatientDTO.PatientDTOBuilder().patientToPatientDTO(patient1));

        authorityRepo.save(patientAuthority);
        authorityRepo.save(adminAuthority);
        authorityRepo.save(patient1Auth);

        // FACILITIES SECTION
        //**************************************************************************************************************
        Facility facility1 = new Facility("Przy Jagiellońskiej", "Wrocław", "214-424-421", 8, 18);
        Facility facility2 = new Facility("Przy Oławskiej", "Wrocław", "421-421-532", 9, 18);
        Facility facility3 = new Facility("Przy Lotniczej", "Wrocław", "950-532-642", 8, 17);
        Facility facility4 = new Facility("Przy Korczaka", "Wrocław", "532-421-531", 8, 16);

        facilityRepo.save(facility1);
        facilityRepo.save(facility2);
        facilityRepo.save(facility3);
        facilityRepo.save(facility4);


        // DOCTORS SECTION
        // *************************************************************************************************************

        Doctor doctor = new Doctor("Janusz", "Nowak", facility1, MedicalSpecialty.GINEKOLOG.toString());
        Doctor doctor1 = new Doctor("Bartosz", "Zadyma", facility1, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor2 = new Doctor("Janina", "Matczak", facility1, MedicalSpecialty.OKULISTA.toString());
        Doctor doctor3 = new Doctor("Klaudia", "Kowalewksa", facility1, MedicalSpecialty.PEDIATRA.toString());
        Doctor doctor4 = new Doctor("Grzegorz", "Grzegorzewski", facility1, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor5 = new Doctor("Mirosław", "Polny", facility1, MedicalSpecialty.KARDIOLOG.toString());
        Doctor doctor6 = new Doctor("Natalia", "Kaus", facility1, MedicalSpecialty.NEFROLOG.toString());
        Doctor doctor7 = new Doctor("Michał", "Marciniak", facility1, MedicalSpecialty.ORTOPEDA.toString());
        Doctor doctor8 = new Doctor("Paweł", "Żbikowski", facility1, MedicalSpecialty.GINEKOLOG.toString());

        Doctor[] doctors1 = new Doctor[] {
                doctor, doctor1, doctor2,
                doctor3, doctor4, doctor5,
                doctor6, doctor7, doctor8 };

        for (Doctor d : doctors1) doctorRepo.save(d);

        var jagiellonskaDoctors = new HashSet<>(Arrays.asList(doctors1));

        Doctor doctor9 = new Doctor("Krzysztof", "Zemanczykiewicz",facility3, MedicalSpecialty.WIRUSOLOG.toString());
        Doctor doctor10 = new Doctor("Filip", "Gruszko", facility3, MedicalSpecialty.PEDIATRA.toString());
        Doctor doctor11 = new Doctor("Katarzyna", "Kozłowska", facility3, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor12 = new Doctor("Oliwia", "Kamińska", facility3, MedicalSpecialty.KARDIOLOG.toString());
        Doctor doctor13 = new Doctor("Sylwia", "Szlachetna", facility3, MedicalSpecialty.DIABETOLOG.toString());
        Doctor doctor14 = new Doctor("Mirosława", "Szymborska", facility3, MedicalSpecialty.ORTOPEDA.toString());
        Doctor doctor15 = new Doctor("Angelina", "Doe", facility3, MedicalSpecialty.INTERNISTA.toString());

        Doctor[] doctors2 = new Doctor[] {
                doctor9, doctor10, doctor11,
                doctor12, doctor13, doctor14,
                doctor15, doctor15 };

        for (Doctor d : doctors2) doctorRepo.save(d);

        var olawskaDoctors = new HashSet<>(Arrays.asList(doctors2));

        Doctor doctor16 = new Doctor("Osama", "Obama", facility2, MedicalSpecialty.PEDIATRA.toString());
        Doctor doctor17 = new Doctor("Donald", "Bush", facility2, MedicalSpecialty.NEFROLOG.toString());
        Doctor doctor18 = new Doctor("Mateusz", "Ponitka", facility2, MedicalSpecialty.OKULISTA.toString());
        Doctor doctor19 = new Doctor("Łukasz", "Piwowarczyk", facility2, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor20 = new Doctor("Paweł", "Pawlak", facility2, MedicalSpecialty.WIRUSOLOG.toString());
        Doctor doctor21 = new Doctor("Sandra", "Kędzior", facility2, MedicalSpecialty.DIABETOLOG.toString());
        Doctor doctor22 = new Doctor("Michalina", "Chrzan", facility2, MedicalSpecialty.ORTOPEDA.toString());
        Doctor doctor23 = new Doctor("Szymon", "Hołownia", facility2, MedicalSpecialty.KARDIOLOG.toString());
        Doctor doctor24 = new Doctor("Pan", "Doktor", facility2, MedicalSpecialty.WIRUSOLOG.toString());

        Doctor[] doctors3 = new Doctor[] {
                doctor16, doctor17, doctor18,
                doctor19, doctor20, doctor21,
                doctor22, doctor23, doctor24 };

        for (Doctor d : doctors3) doctorRepo.save(d);

        var lotniczaDoctors = new HashSet<>(Arrays.asList(doctors3));

        Doctor doctor25 = new Doctor("Dikembe", "Mutombo", facility4, MedicalSpecialty.KARDIOLOG.toString());
        Doctor doctor26 = new Doctor("Poe", "Dameron", facility4, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor27 = new Doctor("Luke", "Skywalker", facility4, MedicalSpecialty.INTERNISTA.toString());
        Doctor doctor28 = new Doctor("Oliwia", "Janiak", facility4, MedicalSpecialty.PEDIATRA.toString());
        Doctor doctor29 = new Doctor("Robert", "Lewandowski", facility4, MedicalSpecialty.PEDIATRA.toString());
        Doctor doctor30 = new Doctor("Master", "Windu", facility4, MedicalSpecialty.GINEKOLOG.toString());
        Doctor doctor31 = new Doctor("Obi-Wan", "Kenobi", facility4, MedicalSpecialty.NEFROLOG.toString());

        Doctor[] doctors4 = new Doctor[] {
                doctor25, doctor26, doctor27,
                doctor28, doctor29, doctor30,
                doctor31 };

        for (Doctor d : doctors4) doctorRepo.save(d);

        var korczakaDoctors = new HashSet<>(Arrays.asList(doctors4));


        List<Doctor> allDoctors = new ArrayList<>(korczakaDoctors);
        allDoctors.addAll(jagiellonskaDoctors);
        allDoctors.addAll(olawskaDoctors);
        allDoctors.addAll(lotniczaDoctors);
        // FREE TERMS SECTION
        // *************************************************************************************************************


        allDoctors.forEach(d -> freeTermService.fillInNewTerms(Date.valueOf("2020-05-18"), 10, d));

//        freeTermService.fillInNewTerms(Date.valueOf("2020-05-16"), 3, doctor6);
//        freeTermService.fillInNewTerms(Date.valueOf("2020-05-16"), 3, doctor27);




    }


    private void fillInNewTerms(Date date, int days, Doctor doctor) {
        freeTermService.fillInNewTerms(date, days, doctor);
    }
}
